import matplotlib.pyplot as plt
import tensorflow as tf
from random import randrange
from math_neural_nets import *

# training data
mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# modify data to the shape we need on input/output
x = x_train.reshape(60000, 784).T
I = np.eye(10)
y = I[:, y_train] # one hot encoded

# additional parameters
bs, ep, eta = 10, 10, .025

# build net
netz = SequentialNet(784)
netz.add_dense(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_highway(30, ReLU())
netz.add_dense(10, ReLU())

# train net
netz.train(x, y, batch_size=5, epochs=5, optim=SGD(eta=eta))
y_tilde = netz.evaluate(x_test.reshape(10000, 784, 1))
guess = np.argmax(y_tilde, 1).T
print("accuracy =", np.sum(guess == y_test)/100)

netz.train(x, y, batch_size=bs, epochs=ep, optim=SGD(eta=0.25))
y_tilde = netz.evaluate(x_test.reshape(10000, 784, 1))
guess = np.argmax(y_tilde, 1).T
print("accuracy =", np.sum(guess == y_test)/100)

netz.train(x, y, batch_size=10, epochs=ep, optim=SGD(eta=0.2))
y_tilde = netz.evaluate(x_test.reshape(10000, 784, 1))
guess = np.argmax(y_tilde, 1).T
print("accuracy =", np.sum(guess == y_test)/100)

netz.train(x, y, batch_size=10, epochs=ep, optim=SGD(eta=0.15))
y_tilde = netz.evaluate(x_test.reshape(10000, 784, 1))
guess = np.argmax(y_tilde, 1).T
print("accuracy =", np.sum(guess == y_test)/100)

netz.train(x, y, batch_size=10, epochs=ep, optim=SGD(eta=0.1))
y_tilde = netz.evaluate(x_test.reshape(10000, 784, 1))
guess = np.argmax(y_tilde, 1).T
print("accuracy =", np.sum(guess == y_test)/100)

# graphical evaluation
for i in range(4):
    k = randrange(y_test.size)
    plt.title('Label is {lb}, guess is {gs}'.format(
        lb=y_test[k], gs=guess[0,k]))
    plt.imshow(x_test[k], cmap='gray')
    plt.show()

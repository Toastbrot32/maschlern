import sys
import numpy as np
import matplotlib.pyplot as plt
from math import atan, sin, cos
from scipy.signal import convolve2d

class Heaviside:
    """initializations of Heaviside-like functions."""

    def wfun(self, m, n):
        return np.random.randn(m, n) * np.sqrt(2 / (m + n))


class SignLike:
    """initializations of sign-like functions."""

    def wfun(self, m, n):
        return np.random.randn(m, n) * np.sqrt(32 / (m + n))


class ReLULike:
    """initializations of ReLU-like functions."""

    def wfun(self, m, n):
        return np.random.randn(m, n) * np.sqrt(4 / (m + n))


class AbsLike:
    """initializations of abs-like functions."""

    def wfun(self,m ,n):
        return np.random.randn(m, n) * np.sqrt(1 / (m + n))


class sigmoid(Heaviside):
    """logistic activation function."""

    def __init__(self):
        self.name = 'sigmoid'

    def evaluate(self, x):
        """evaluate."""
        y = 1/(1+np.exp(-x))
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = 1/(1+np.exp(-x))
        return y*(1-y)


class tanh(SignLike):
    """hyperbolic tangent activation function."""

    def __init__(self):
        self.name = 'tanh'

    def evaluate(self, x):
        """evaluate."""
        y = np.tanh(x)
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = np.tanh(x)
        return 1-y**2


class ReLU(ReLULike):
    """rectified linear unit activation function."""

    def __init__(self):
        self.name = 'ReLU'

    def evaluate(self, x):
        """evaluate."""
        y = x.clip(min=0)
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = (x >= 0)*1.0
        return y


class leakyReLU(ReLULike):
    """leaky ReLU activation function."""

    def __init__(self):
        self.name = 'leaky ReLU'

    def evaluate(self, x):
        """evaluate."""
        y = x.clip(min=0)+.01*x.clip(max=0)
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = (x >= 0)*1.0 + (x < 0)*.01
        return y

class ELU(ReLULike):
    """exponential linear unit activation function."""

    def __init__(self):
        self.name = 'ELU'

    def evaluate(self, x):
        """evaluate."""
        y = x.clip(min=0) + (np.exp(x.clip(max=0))-1)
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = (x >= 0)*1.0 + (x < 0)*np.exp(x)
        return y


class SoftPlus(ReLULike):
    """SoftPlus activation function."""

    def __init__(self):
        self.name = 'SoftPlus'

    def evaluate(self, x):
        """evaluate."""
        y = np.log(1+np.exp(x))
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = 1/(1+np.exp(-x))
        return y


class absval(AbsLike):
    """absolute value activation function."""

    def __init__(self):
        self.name = 'absolute value'

    def evaluate(self, x):
        """evaluate."""
        y = np.abs(x)
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = np.sign(x)
        return y


class logcosh(AbsLike):
    """log of hyperbolic cosine activation function."""

    def __init__(self):
        self.name = 'log of cosh'

    def evaluate(self, x):
        """evaluate."""
        y = np.log(np.cosh(x))
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = np.tanh(x)
        return y


class exp(ReLULike):
    """a silly activation function."""

    def __init__(self):
        self.name = 'exp'

    def evaluate(self, x):
        """evaluate."""
        y = np.exp(x)
        return y

    def derive(self, x):
        """evaluate derivative."""
        y = np.exp(x)
        return y

class SGD:
    """SGD with minibatch variants"""

    def __init__(self, eta=.1, momentum=0.0, nesterov=False):
        self.eta = eta
        self.momentum = momentum
        self.nesterov = nesterov # not implemented yet


class Adam:
    """Adam: optimized SGD"""

    def __init__(self, eta=.001, beta1=.9, beta2=.999, epsilon=1e-8):
        self.eta = eta
        self.beta1 = beta1
        self.beta2 = beta2
        self.epsilon = epsilon


class DenseLayer():
    """Dense layer of a sequential net"""

    def __init__(self, ni, no, afun=ReLU()):
        self.__a = None
        self.W = afun.wfun(no, ni)
        self.dW = np.zeros_like(self.W)
        self.b = np.zeros((no, 1))
        self.db = np.zeros_like(self.b)
        self.__z = None
        self.afun = afun

    def evaluate(self, a):
        self.__a = a
        self.__z = self.W @ a + self.b
        return self.afun.evaluate(self.__z)

    def backprop(self, delta):
        delta = self.afun.derive(self.__z) * delta
        self.dW = delta @ self.__a.T
        self.db = delta @ np.ones( (delta.shape[1],1) )
        return self.W.T @ delta
        #return self.W.T @ delta, np.zeros_like(self.W.T @ delta), np.zeros_like(self.W.T @ delta)

    def update(self, optim):
        if type(optim) == SGD:
            self.W -= optim.eta * self.dW
            self.b -= optim.eta * self.db


class HighwayLayer():

    def __init__(self, ni, no, afun=ReLU()):
        self.coupled = False
        self.__a = None

        self.W = afun.wfun(no, ni)
        self.WT = afun.wfun(no, ni)
        self.WC = afun.wfun(no, ni)

        self.dW = np.zeros_like(self.W)
        self.dWT = np.zeros_like(self.W)
        self.dWC = np.zeros_like(self.W)

        self.b = np.zeros((no, 1))
        self.bT = -1*np.ones((no, 1))
        self.bC = np.zeros((no, 1))

        self.db = np.zeros_like(self.b)
        self.dbT = np.zeros_like(self.b)
        self.dbC = np.zeros_like(self.b)

        self.__z = None
        self.__zT = None
        self.__zC = None
        self.afun = afun
        self.afunT = sigmoid()
        self.afunC = sigmoid()

    def evaluate(self, a):
        self.__a = a
        self.__z = self.W @ a + self.b
        self.__zT = self.WT @ a + self.bT
        if self.coupled == False:
            self.__zC = self.WC @ a + self.bC
        else:
            self.__zC = np.ones_like(self.__zT) - self.__zT
        return self.afun.evaluate(self.__z) * self.afunT.evaluate(self.__zT) + self.__a * self.afunC.evaluate(self.__zC)

    def backprop(self, delta):
        deltao = delta
        deltaT = self.afun.evaluate(self.__z) * self.afunT.derive(self.__zT) * delta
        if self.coupled == False:
            deltaC = self.__a * self.afunC.derive(self.__zC) * delta
        else:
            deltaC = np.ones_like(deltaT) - deltaT
        delta = self.afun.derive(self.__z) * self.afunT.evaluate(self.__zT) * delta
        self.dW = delta @ self.__a.T
        self.db = delta @ np.ones( (delta.shape[1],1) )
        self.dWT = deltaT @ self.__a.T
        self.dbT = deltaT @ np.ones( (delta.shape[1],1) )
        self.dWC = deltaC @ self.__a.T
        self.dbC = deltaC @ np.ones( (delta.shape[1],1) )
        return self.W.T @ delta + self.WT.T @ deltaT +  self.afunC.evaluate(self.__zC)*deltao + self.WC.T @ deltaC



    def update(self, optim):
        if type(optim) == SGD:
            self.W -= optim.eta * self.dW
            self.b -= optim.eta * self.db
            self.WT -= optim.eta * self.dWT
            self.bT -= optim.eta * self.dbT
            self.WC -= optim.eta * self.dWC
            self.bC -= optim.eta * self.dbC


class SequentialNet:
    """Sequential neural network class."""

    def __init__(self, n):
        self.layers = []
        self.no = np.array(n)

    def add_dense(self, n, afun=ReLU()):
        n = np.array(n)
        self.layers.append( DenseLayer(self.no, n, afun) )
        self.no = n

    def add_highway(self, n, afun=ReLU()):
        n = np.array(n)
        self.layers.append( HighwayLayer(self.no, n, afun) )
        self.no = n

    def evaluate(self, x, l=0):
        j=1
        for layer in self.layers:
            if not j==l:
                x = layer.evaluate(x)
            j=j+1
        return x

    def backprop(self, x, y):
        k = y.shape[1]
        a = self.evaluate(x)
        sys.stdout.write("loss: %f\r" % (np.linalg.norm(a-y)) )
        sys.stdout.flush()
        delta = (a-y)/k
        for layer in self.layers[::-1]:
            delta = layer.backprop(delta)

    def train(self, x, y, batch_size=16, epochs=10, optim=SGD()):
        n_data = y.shape[1]
        number_of_batches = int(np.ceil(n_data/batch_size))
        for e in range(epochs):
            print("epoch %3d of %3d" % (e+1, epochs))
            p = np.random.permutation(n_data)
            for j in range(number_of_batches):
                self.backprop(
                    x[:,p[j*batch_size:(j+1)*batch_size]],
                    y[:,p[j*batch_size:(j+1)*batch_size]])
                for layer in self.layers:
                    layer.update(optim)


class FeedforwardNet:
    """Feedforward neural network class."""

    def __init__(self, layers, afun=ReLU()):
        self.layers = np.array(layers)
        self.afuns = [afun] * (len(layers)-1)
        self.weights = [afun.wfun(m, n) for m, n in
                        zip(layers[1:], layers[:-1])]
        self.biases = [np.zeros((m, 1)) for m in layers[1:]]
        self.__a = [None for m in layers]
        self.__z = [None for m in layers[1:]]

    def get_z(self):
        return self.__z

    def get_a(self):
        return self.__a

    def evaluate(self, x):
        self.__a[0] = x
        ell = 0
        for afun, W, b in zip(self.afuns, self.weights, self.biases):
            self.__z[ell] = W @ self.__a[ell] + b
            self.__a[ell+1] = afun.evaluate(self.__z[ell])
            ell += 1
        y = self.__a[ell]
        return y

    def backprop(self, x, y):
        dW = [None for W in self.weights]
        db = [None for b in self.biases]
        k = y.shape[1]
        e = np.ones((k,1))
        y_tilde = self.evaluate(x)
        sys.stdout.write("loss: %f\r" % (np.linalg.norm(y-y_tilde)) )
        sys.stdout.flush()
        delta = self.afuns[-1].derive(self.__z[-1]) * (y_tilde-y)/k
        dW[-1] = delta @ (self.__a[-2].T)
        db[-1] = delta @ e
        for i in reversed(range(len(self.layers)-2)):
            delta = self.afuns[i].derive(self.__z[i]) \
                * (self.weights[i+1].T @ delta)
            dW[i] = delta @ (self.__a[i].T)
            db[i] = delta @ e
        return dW, db

    def train(self, x, y, batch_size=16, epochs=10, optim=SGD()):
        ell = len(self.layers)-1
        n_data = y.shape[1]
        number_of_batches = int(np.ceil(n_data/batch_size))
        # initialize v (momentum + Adam), w (Adam)
        vW = [np.zeros_like(W) for W in self.weights]
        vb = [np.zeros_like(b) for b in self.biases]
        if type(optim) == Adam:
            wW = [np.zeros_like(W) for W in self.weights]
            wb = [np.zeros_like(b) for b in self.biases]
            k = 0
        for e in range(epochs):
            print("epoch %3d of %3d" % (e+1, epochs))
            p = np.random.permutation(n_data)
            for j in range(number_of_batches):
                dW, db = self.backprop(
                                  x[:,p[j*batch_size:(j+1)*batch_size]],
                                  y[:,p[j*batch_size:(j+1)*batch_size]])
                if type(optim) == SGD:
                    for i in range(ell):
                        vW[i] = vW[i]*optim.momentum \
                            + dW[i]*(1-optim.momentum)
                        vb[i] = vb[i]*optim.momentum \
                            + db[i]*(1-optim.momentum)
                        self.weights[i] -= optim.eta*vW[i]
                        self.biases[i] -= optim.eta*vb[i]
                if type(optim) == Adam:
                    k += 1
                    alpha = optim.eta * \
                        np.sqrt(1-optim.beta2**k)/(1-optim.beta1**k)
                    for i in range(ell):
                        vW[i] = vW[i]*optim.beta1 \
                            + dW[i]*(1-optim.beta1)
                        vb[i] = vb[i]*optim.beta1 \
                            + db[i]*(1-optim.beta1)
                        wW[i] = wW[i]*optim.beta2 \
                            + dW[i]**2*(1-optim.beta2)
                        wb[i] = wb[i]*optim.beta2 \
                            + db[i]**2*(1-optim.beta2)
                        self.weights[i] -= alpha \
                            * (vW[i] / np.sqrt(wW[i]+optim.epsilon))
                        self.biases[i] -= alpha \
                            * (vb[i] / np.sqrt(wb[i]+optim.epsilon))


    def draw(self):
        num_layers = len(self.layers)
        max_neurons_per_layer = np.amax(self.layers)
        dist = 2*max(1,max_neurons_per_layer/num_layers)
        y_shift = self.layers/2-.5
        rad = .3
        
        fig = plt.figure(frameon=False)
        ax = fig.add_axes([0, 0, 1, 1])
        ax.axis('off')

        for i in range(num_layers):
            if i > 0:
                b_max = abs(self.biases[i-1]).max()
                b_max = max(b_max,np.finfo(float).eps)
            for j in range(self.layers[i]):
                if i > 0:
                    b = self.biases[i-1][j]
                    if b < 0:
                        c = 'b'
                    else:
                        c = 'r'
                    circle = plt.Circle((i*dist, y_shift[i]-j),
                                        radius=rad, color=c,
                                        alpha=abs(b)/b_max)
                    ax.add_patch(circle)
                circle = plt.Circle((i*dist, y_shift[i]-j),
                                    radius=rad, fill=False)
                ax.add_patch(circle)

        for i in range(num_layers-1):
            w_max = np.absolute(self.weights[i]).argmax()
            w_max = max(w_max,np.finfo(float).eps)
            for j in range(self.layers[i]):
                for k in range(self.layers[i+1]):
                    angle = atan(float(k-j+y_shift[i]-y_shift[i+1]) / dist)
                    x_adjust = rad * cos(angle)
                    y_adjust = rad * sin(angle)
                    w = self.weights[i][k,j]
                    if w < 0:
                        c = 'b'
                    else:
                        c = 'r'
                    line = plt.Line2D((i*dist+x_adjust,
                                       (i+1)*dist-x_adjust),
                                      (y_shift[i]-j-y_adjust,
                                       y_shift[i+1]-k+y_adjust),
                                      lw=abs(w), color=c)
                    ax.add_line(line)

        ax.axis('scaled')

from FeedforwardNetwork import FeedforwardNet
from mlxtend.data import loadlocal_mnist
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import copy

X, y = loadlocal_mnist(
        images_path='train-images-idx3-ubyte', 
        labels_path='train-labels-idx1-ubyte')


Xtest, ytest = loadlocal_mnist(
        images_path='t10k-images-idx3-ubyte', 
        labels_path='t10k-labels-idx1-ubyte')

samples=60000

Xdata = X[0:samples].reshape(samples,28*28)/256
ydata=np.zeros((samples,10))
for i in range(0,samples):
    ydata[:][i] = np.eye(1,10,y[i])

A=FeedforwardNet([784,100,10],4*["logistic"])
E=copy.deepcopy(A)
B=copy.deepcopy(A)
for k in range(0,100):
    A.train(np.transpose(Xdata), np.transpose(ydata), eta=0.001, batch_size=20, epochs=1, GDVersion="Adagrad")
    B.train(np.transpose(Xdata), np.transpose(ydata), eta=0.001, batch_size=20, epochs=1, GDVersion="RMSprop")
    E.train(np.transpose(Xdata), np.transpose(ydata), eta=0.001, batch_size=20, epochs=1, GDVersion="Adam",Adam_beta1=0.89, Adam_beta2=0.99)
    tests=10000
    hitsada=0
    miss=[]
    for j in range(0,tests):
        testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
        if np.argmax(A.evaluate(testdata)) == ytest[j]:
            hitsada=hitsada+1
        else:
            miss.append(j)
    hitsadam=0
    for j in range(0,tests):
        testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
        if np.argmax(E.evaluate(testdata)) == ytest[j]:
            hitsadam=hitsadam+1
        else:
            miss.append(j)
    hitsrms=0
    for j in range(0,tests):
        testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
        if np.argmax(B.evaluate(testdata)) == ytest[j]:
            hitsrms=hitsrms+1
        else:
            miss.append(j)
    print(f"Adam {hitsadam/tests} Adagrad {hitsada/tests} RMSprop {hitsrms/tests}")

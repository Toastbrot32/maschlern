from FeedforwardNetwork import FeedforwardNet
from mlxtend.data import loadlocal_mnist
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import copy

X, y = loadlocal_mnist(
        images_path='train-images-idx3-ubyte', 
        labels_path='train-labels-idx1-ubyte')


Xtest, ytest = loadlocal_mnist(
        images_path='t10k-images-idx3-ubyte', 
        labels_path='t10k-labels-idx1-ubyte')

samples=20000

Xdata = X[0:samples].reshape(samples,28*28)/256
ydata=np.zeros((samples,10))
for i in range(0,samples):
    ydata[:][i] = np.eye(1,10,y[i])

num_epochs=5
b=10

A=FeedforwardNet([784,100,10],3*["ELU"])
B=copy.deepcopy(A)
C=copy.deepcopy(A)
D=copy.deepcopy(A)
E=copy.deepcopy(A)
A.train(np.transpose(Xdata), np.transpose(ydata), eta=0.1, batch_size=b, epochs=num_epochs, GDVersion="SGD")
tests=10000
hits=0
for j in range(0,tests):
    testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
    if np.argmax(A.evaluate(testdata)) == ytest[j]:
         hits=hits+1
print(f"SGD {hits/tests}")

#B=FeedforwardNet([784,100,10],3*["ELU"])
B.train(np.transpose(Xdata), np.transpose(ydata), eta=0.001, batch_size=b, epochs=num_epochs, GDVersion="SGDM")
tests=10000
hits=0
for j in range(0,tests):
    testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
    if np.argmax(B.evaluate(testdata)) == ytest[j]:
        hits=hits+1
print(f"SGDM {hits/tests}")

#C=FeedforwardNet([784,100,10],3*["ELU"])
C.train(np.transpose(Xdata), np.transpose(ydata), eta=0.01, batch_size=b, epochs=num_epochs, GDVersion="Adagrad")
tests=10000
hits=0
for j in range(0,tests):
    testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
    if np.argmax(C.evaluate(testdata)) == ytest[j]:
        hits=hits+1
print(f"Adagrad {hits/tests}")
        

#D=FeedforwardNet([784,100,10],3*["ELU"])
D.train(np.transpose(Xdata), np.transpose(ydata), eta=0.001, batch_size=b, epochs=num_epochs, GDVersion="RMSprop")
tests=10000
hits=0
for j in range(0,tests):
    testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
    if np.argmax(D.evaluate(testdata)) == ytest[j]:
        hits=hits+1
print(f"RMSprop {hits/tests}")

#E=FeedforwardNet([784,100,10],3*["ELU"])
E.train(np.transpose(Xdata), np.transpose(ydata), eta=0.0012, batch_size=b, epochs=num_epochs, GDVersion="Adam")
tests=10000
hits=0
miss=[]
for j in range(0,tests):
    testdata=np.transpose(np.array(Xtest[j].reshape(1,28*28))/256)
    if np.argmax(E.evaluate(testdata)) == ytest[j]:
        hits=hits+1
    else:
        miss.append(j)
print(f"Adam {hits/tests}")


for i in range (0,100):
    #plt.image.imsave(f"failed{i}.png", np.array(Xtest[miss[i]].reshape(28,28)),cmap='gray')
    failed=np.transpose(np.array(Xtest[miss[i]].reshape(1,28*28))/256)
    print(miss[i],np.argmax(E.evaluate(failed)), np.max(E.evaluate(failed)), ytest[miss[i]])

    fig = plt.figure()
    plt.imshow(np.array(Xtest[miss[i]].reshape(28,28)), cmap='gray')
    plt.xlabel(f'Guessed {np.argmax(E.evaluate(failed))} was {ytest[miss[i]]} confidence {np.max(E.evaluate(failed))}')
    plt.savefig(f'failed{i}.png', bbox_inches='tight')

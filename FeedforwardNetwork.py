import numpy as np
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
import scipy.special as sp

class FeedforwardNet:
    def __init__(self, n, afuns):
        m = np.size(n)

        self.n = n
        self.costfun = lambda x,y:((np.linalg.norm(x-y)**2)/2)
        self.leaky_ReLU_alpha=0.001
        self.eLU_alpha=0.001
        self.pafuns = {\
            "Heaviside" : lambda x:(np.heaviside(x,1)),\
            "Mod_Heaviside" : lambda x:(np.heaviside(x,1)),\
            "logistic" : lambda x:(sp.expit(x)),\
            "sign" : lambda x:(np.sign(x)),\
            "tanh" : lambda x:(np.tanh(x)),\
            "ReLU" : lambda x:(np.maximum(0,x)),\
            "leaky_ReLU" : lambda x:(np.maximum(0,x) + np.minimum(0,self.leaky_ReLU_alpha*x)),\
            "ELU" : lambda x:(np.where(x>0,x,0) + self.eLU_alpha*(np.exp(np.where(x<=0,x,0))-1)),\
            "SoftPlus" : lambda x:(np.log(1+np.exp(x))),\
            "abs" : lambda x:(np.abs(x)),\
            }
        self.pderafuns = {\
            "Heaviside" : lambda x:(0),\
            "Mod_Heaviside" : lambda x:(0),\
            "logistic" : lambda x:(sp.expit(x)*(1-sp.expit(x))),\
            "sign" : lambda x:(0),\
            "tanh" : lambda x:(1-np.tanh(x)**2),\
            "ReLU" : lambda x:(np.heaviside(x,1)),\
            "leaky_ReLU" : lambda x:(np.heaviside(x,1) + self.leaky_ReLU_alpha*np.heaviside(-x,1)),\
            "ELU" : lambda x:(np.where(x>0,1,0) + self.eLU_alpha*(np.exp(np.where(x<=0,x,0)))),\
            "SoftPlus" : lambda x:(sp.expit(x)),\
            "abs" : lambda x:(np.heaviside(x,1) - np.heaviside(-x,1)),\
            }

        self.afuns = []
        for i in range(0, m-1):
            self.afuns.append(self.pafuns[afuns[i]])

        self.derafuns = []
        for i in range(0, m-1):
            self.derafuns.append(self.pderafuns[afuns[i]])

        self.biasvecs = []
        for i in range(1, m):
            self.biasvecs.append(np.zeros((n[i],1)))

        self.weights = []
        for i in range(0, m-1):
            self.weights.append(np.random.randn(n[i+1], n[i]) * np.sqrt(4 / (n[i+1] + n[i])))

    def evaluate(self,x):
        for i in range(0,np.size(self.n)-1):
            x=self.weights[i]@x+self.biasvecs[i]
            x=self.afuns[i](x)
        return x

    def backprop(self, x, y):
        batch_size=x.shape[1]
        m = len(self.n)-1
        z = [None]*(m+1)
        a = [None]*(m+1)
        a[0] = x
        for i in range(0,m):
            x = self.weights[i]@x+self.biasvecs[i]
            z[i] = x
            x = self.afuns[i](x)
            a[i+1] = x
        delta = [None]*m
        delta[m-1] = self.derafuns[m-1](z[m-1])*(a[m]-y)
        for i in reversed(range(0,m-1)):
            delta[i] = self.derafuns[i](z[i])*(np.transpose(self.weights[i+1])@delta[i+1])
        dCdb = [None]*m
        dCdW = [None]*m
        for i in range(0,m):
            dCdW[i] = (delta[i]@np.transpose(a[i]))/batch_size
            dCdb[i] = (delta[i]@np.ones((batch_size,1)))/batch_size
        return dCdb, dCdW

    def train(self, x, y, eta, batch_size, epochs, GDVersion="SGD", SGDM_gamma=0.9, RMSprop_gamma=0.9, Adam_beta1=0.9, Adam_beta2=0.999):
        num_batches = int(np.ceil(y.shape[1]/batch_size))
        if GDVersion=="SGD":
            for i in range(0,epochs):
                print(f'SGD epoch {i+1} von {epochs} Epochen', end="\r")
                p=np.random.permutation(y.shape[1])
                for k in range(0,num_batches):
                    (dCdb,dCdW)=self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                    for j in range(0,len(self.n)-1):
                        self.weights[j]=self.weights[j]-eta*dCdW[j]
                        self.biasvecs[j]=self.biasvecs[j]-eta*dCdb[j]

        if GDVersion=="SGDM":
            vW=[None]*len(self.n)
            vb=[None]*len(self.n)

            for i in range(0,epochs):
                print(f'SGDM epoch {i+1} von {epochs} Epochen', end="\r")
                p=np.random.permutation(y.shape[1])
                for i in range(0,len(self.n)-1):
                    vW[i]=np.zeros(self.weights[i].shape)
                    vb[i]=np.zeros(self.biasvecs[i].shape)

                for k in range(0,num_batches):
                    (dCdb,dCdW)=self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                    for j in range(0,len(self.n)-1):
                        vW[j] = SGDM_gamma*vW[j] + dCdW[j]*eta
                        self.weights[j]=self.weights[j] - vW[j]
                        vb[j] = SGDM_gamma*vb[j] + dCdb[j]*eta
                        self.biasvecs[j]=self.biasvecs[j] - vb[j]

        if GDVersion=="NAG":#???
            for i in range(0,epochs):
                print(f'NAG epoch {i} von {epochs} Epochen')
                vW=[None]*len(self.n)
                vb=[None]*len(self.n)

                for i in range(0,len(self.n)-1):
                    vW[i]=np.zeros(self.weights[i].shape)
                    vb[i]=np.zeros(self.biasvecs[i].shape)
                (dCdb,dCdW)=self.backprop(x,y)
                for j in range(0,len(self.n)-1):
                    vW[j] = SGDM_gamma*vW[j] + (dCdW[j]*eta)/batch_size
                    self.weights[j]=self.weights[j]-vW[j]
                    vb[j] = SGDM_gamma*vb[j] + (dCdb[j]@np.ones((batch_size,1))*eta)/batch_size
                    self.biasvecs[j]=self.biasvecs[j]-vb[j]

        if GDVersion=="Adagrad":
            eps=1e-8
            gWs = [None]*(len(self.n)-1)
            gbs = [None]*(len(self.n)-1)

            for i in range(0,epochs):
                print(f'Adagrad epoch {i+1} von {epochs} Epochen', end="\r")
                p=np.random.permutation(y.shape[1])
                for i in range(0,len(self.n)-1):
                    gWs[i] = np.zeros(self.weights[i].shape)
                    gbs[i] = np.zeros(self.biasvecs[i].shape)

                for k in range(0,num_batches):
                    (dCdb,dCdW)=self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                    for j in range(0,len(self.n)-1):
                        gWs[j] = gWs[j] + np.square(dCdW[j])
                        gbs[j] = gbs[j] + np.square(dCdb[j])
                    for j in range(0,len(self.n)-1):
                        self.weights[j] = self.weights[j] - np.reciprocal(np.sqrt(gWs[j] + eps))*eta*dCdW[j]
                        self.biasvecs[j] = self.biasvecs[j] - np.reciprocal(np.sqrt(gbs[j] + eps))*eta*dCdb[j]

        if GDVersion=="Adagrad2":
            eps=1e-8
            gWs = [None]*(len(self.n)-1)
            gbs = [None]*(len(self.n)-1)
            for i in range(0,len(self.n)-1):
                gWs[i] = np.zeros(self.weights[i].shape)
                gbs[i] = np.zeros(self.biasvecs[i].shape)

            for i in range(0,epochs):
                print(f'Adagrad epoch {i+1} von {epochs} Epochen', end="\r")
                p=np.random.permutation(y.shape[1])

                for k in range(0,num_batches):
                    (dCdb,dCdW)=self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                    for j in range(0,len(self.n)-1):
                        gWs[j] = gWs[j] + np.square(dCdW[j])
                        gbs[j] = gbs[j] + np.square(dCdb[j])
                    for j in range(0,len(self.n)-1):
                        self.weights[j] = self.weights[j] - np.reciprocal(np.sqrt(gWs[j] + eps))*eta*dCdW[j]
                        self.biasvecs[j] = self.biasvecs[j] - np.reciprocal(np.sqrt(gbs[j] + eps))*eta*dCdb[j]

        if GDVersion=="RMSprop":
            eps=1e-8
            wW = [None]*(len(self.n)-1)
            wb = [None]*(len(self.n)-1)

            for i in range(0,epochs):
                print(f'RMSprop epoch {i+1} von {epochs} Epochen', end="\r")
                p=np.random.permutation(y.shape[1])
                for i in range(0,len(self.n)-1):
                    wW[i] = np.zeros(self.weights[i].shape)
                    wb[i] = np.zeros(self.biasvecs[i].shape)

                for k in range(0,num_batches):
                    (dCdb,dCdW)=self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                    for j in range(0,len(self.n)-1):
                        wW[j] = wW[j]*RMSprop_gamma + np.square(dCdW[j])*(1-RMSprop_gamma)
                        wb[j] = wb[j]*RMSprop_gamma + np.square(dCdb[j])*(1-RMSprop_gamma)
                    for j in range(0,len(self.n)-1):
                        self.weights[j] = self.weights[j] - np.reciprocal(np.sqrt(wW[j] + eps))*eta*dCdW[j]
                        self.biasvecs[j] = self.biasvecs[j] - np.reciprocal(np.sqrt(wb[j] + eps))*eta*dCdb[j]

        if GDVersion=="Adam":
            eps=1e-8
            wW = [None]*len(self.n)
            wb = [None]*len(self.n)
            vW = [None]*len(self.n)
            vb = [None]*len(self.n)

            for l in range(0,epochs):
                print(f'Adam epoch {l+1} von {epochs} Epochen', end="\r")
                p=np.random.permutation(y.shape[1])
                for i in range(0,len(self.n)-1):
                    wW[i] = np.zeros(self.weights[i].shape)
                    wb[i] = np.zeros(self.biasvecs[i].shape)
                for i in range(0,len(self.n)-1):
                    vW[i] = np.zeros(self.weights[i].shape)
                    vb[i] = np.zeros(self.biasvecs[i].shape)

                for k in range(1,num_batches-1):
                    (dCdb,dCdW)=self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                    for j in range(0,len(self.n)-1):
                        vW[j] = vW[j]*Adam_beta1 + dCdW[j]*(1-Adam_beta1)
                        vb[j] = vb[j]*Adam_beta1 + dCdb[j]*(1-Adam_beta1)
                    for j in range(0,len(self.n)-1):
                        wW[j] = wW[j]*Adam_beta2 + np.square(dCdW[j])*(1-Adam_beta2)
                        wb[j] = wb[j]*Adam_beta2 + np.square(dCdb[j])*(1-Adam_beta2)
                    alpha=eta*(np.sqrt(1-Adam_beta2**k)/(1-Adam_beta1**k))
                    epsh=eps/(1-Adam_beta2**k)
                    for j in range(0,len(self.n)-1):
                        self.weights[j] = self.weights[j] - np.reciprocal(np.sqrt(wW[j] + epsh))*alpha*vW[j]
                        self.biasvecs[j] = self.biasvecs[j] - np.reciprocal(np.sqrt(wb[j] + epsh))*alpha*vb[j]

        if GDVersion=="Adam2":
            eps=1e-8
            wW = [None]*len(self.n)
            wb = [None]*len(self.n)
            vW = [None]*len(self.n)
            vb = [None]*len(self.n)
            for i in range(0,len(self.n)-1):
                wW[i] = np.zeros(self.weights[i].shape)
                wb[i] = np.zeros(self.biasvecs[i].shape)
            for i in range(0,len(self.n)-1):
                vW[i] = np.zeros(self.weights[i].shape)
                vb[i] = np.zeros(self.biasvecs[i].shape)

            for l in range(0,epochs):
                print(f'Adam epoch {l+1} von {epochs} Epochen', end="\r")
                p=np.random.permutation(y.shape[1])

                for k in range(1,num_batches-1):
                    (dCdb,dCdW)=self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                    for j in range(0,len(self.n)-1):
                        vW[j] = vW[j]*Adam_beta1 + dCdW[j]*(1-Adam_beta1)
                        vb[j] = vb[j]*Adam_beta1 + dCdb[j]*(1-Adam_beta1)
                    for j in range(0,len(self.n)-1):
                        wW[j] = wW[j]*Adam_beta2 + np.square(dCdW[j])*(1-Adam_beta2)
                        wb[j] = wb[j]*Adam_beta2 + np.square(dCdb[j])*(1-Adam_beta2)
                    alpha=eta*(np.sqrt(1-Adam_beta2**k)/(1-Adam_beta1**k))
                    epsh=eps/(1-Adam_beta2**k)
                    for j in range(0,len(self.n)-1):
                        self.weights[j] = self.weights[j] - np.reciprocal(np.sqrt(wW[j] + epsh))*alpha*vW[j]
                        self.biasvecs[j] = self.biasvecs[j] - np.reciprocal(np.sqrt(wb[j] + epsh))*alpha*vb[j]
        print("                                                                   ", end="\r")

    def draw(self, **kwargs):
        fig, ax = plt.subplots()
        for j in range(0,np.size(self.n)):
            for i in range(0, self.n[j]):
                pos = (4 * j, (4 * i) - 2 * self.n[j])
                if not j == np.size(self.n)-1:
                    for h in range (0, self.n[j+1]):
                        pos2 = (4 * (j+1), (4 * h) - 2 * self.n[j+1])
                        line = plt.Line2D((pos[0],pos2[0]), (pos[1],pos2[1]), lw = 2*min(1,abs(self.weights[j][h,i])), zorder = 0.0, color = [(np.sign(self.weights[j][h,i])+1)*0.5,0,(-np.sign(self.weights[j][h,i])+1)*0.5],)
                        pos3 = np.array(pos) + ((h+1)/(self.n[j+1]+1)) * (np.array(pos2) - np. array(pos))
                        ax.add_line(line)
                        if 'PrintAnnotations' in kwargs:
                            if kwargs['PrintAnnotations']==True:
                                ax.annotate(str(round(self.weights[j][h,i],2)), pos3, zorder = 100.0)
                if not j == 0:
                    circle1 = plt.Circle(pos, radius = 0.515, fc=[1,1,1], ec=[0,0,0], zorder = 10.0)
                    circle2 = plt.Circle(pos, radius = 0.5, fc = [(np.sign(self.biasvecs[j-1][i,0])+1)*0.5,0,(-np.sign(self.biasvecs[j-1][i,0])+1)*0.5], alpha=min(1,abs(self.biasvecs[j-1][i,0])), ec=[1,1,1], zorder = 10.0)
                    ax.add_patch(circle1)
                    ax.add_patch(circle2)
                else:
                    circle1 = plt.Circle(pos, radius = 0.515, fc='g', ec=[0,0,0], zorder = 10.0)
                    ax.add_patch(circle1)
                if 'PrintAnnotations' in kwargs:
                    if kwargs['PrintAnnotations']==True:
                        if not j == 0:
                            ax.annotate(str(round(self.biasvecs[j-1][i,0],2)), pos, zorder = 1000.0)
        plt.axis('scaled')
        plt.savefig('NN.png', dpi=2000)
        plt.show()

# A=FeedforwardNet([2,10,1],3*["ELU"])
# #print(A.afuns)
# #print(A.biasvecs)
# #print(A.weights)
# #print(A.afuns[0]([1,2,3]))
# #print(A.evaluate(np.ones((2,1))))
# x=np.array([[0., 0., 1., 1.],
#             [0., 1., 0., 1.]])
# y=np.array([0., 1., 1., 1.])
# A.train( x, y, eta=0.001, batch_size=4, epochs=7000, GDVersion="Adam")
# A.draw(PrintAnnotations=False)
# print("Adam",A.evaluate(x))

# x=np.array([[0., 0., 1., 1.],
#             [0., 1., 0., 1.]])
# y=np.array([0., 1., 1., 1.])
# A.train( x, y, eta=0.001, batch_size=4, epochs=7000, GDVersion="RMSprop")
# A.draw(PrintAnnotations=False)
# print("RMSprop",A.evaluate(x))

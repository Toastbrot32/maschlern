import matplotlib.pyplot as plt
plt.style.use('ggplot')
#import tensorflow as tf
from random import randrange
from math_neural_nets import *

# build net
c = np.random.rand(1,1)[0][0] * 100
y = []
N = 1000
h = 1/N
netz = SequentialNet(1)
for n in range(0,N):
    netz.add_res(1, np.array([[-2*h*np.tan(n*h)]]), np.array([[0.0]]))

x0 = np.array([c])
u = netz.evaluate(x0) - x0 * np.cos(1)**2
print(u)


s = 1000
x = np.random.rand(s,1).T
y = x * np.cos(1)**2

w = netz.get_res()

plt.subplot(2,1,1)
plt.ylabel('Weights vorher (r) nachher (g)')
plt.xlabel('Layer')
plt.plot(w, 'r')

netz.train(x,y,batch_size=1,epochs=5,optim=SGD(eta=0.1*h))

u = netz.evaluate(x0) - x0 * np.cos(1)**2

print(u)

w = netz.get_res()

plt.plot(w, 'g')

plt.subplot(2,1,2)
v = []
for l in range(0,N):
    v.append(np.abs(netz.evaluate(x0,l) - np.cos(1)**2)[0][0])
plt.ylabel('Dropped layer')
plt.xlabel('Layer')
plt.plot(v, 'r')
plt.show()

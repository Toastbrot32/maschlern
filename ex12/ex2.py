import matplotlib.pyplot as plt
plt.style.use('ggplot')
#import tensorflow as tf
from random import randrange
from math_neural_nets import *

# build net
c=1
x = [5,6,7,8,9,10,20,30,40,50,60,70,80,90,100,200,300,400,500,1000,2000,4000,6000,8000,10000,20000,40000,60000,80000,100000]
x = [5,6,7,8,9,10,20,30,40,50,60,70,80,90,100,200,300,400,500,1000,2000]
y = []
for N in x:
    h = 1/N
    netz = SequentialNet(1)

    for n in range(0,N):
        netz.add_res(1, np.array([[-2*h*np.tan(n*h)]]), np.array([0]))

    x0 = np.array([c])
    u = netz.evaluate(x0) - x0 * np.cos(1)**2
    y.append(u)
    print("Unterschied:", u[0], "Layers", N)

w = netz.get_res()

plt.subplot(3,1,1)
plt.xlabel('Result')
plt.ylabel('Number of layers')
plt.plot(x, y, 'r-o')
plt.yscale('log')

plt.subplot(3,1,2)
plt.ylabel('Weights')
plt.xlabel('Layer')
plt.plot(w, 'r')

plt.subplot(3,1,3)
v = []
x0 = np.array([1])
N = 1000
h = 1/N
netz = SequentialNet(1)
for n in range(0,N):
    netz.add_res(1, np.array([[-2*h*np.tan(n*h)]]), np.array([0]))

for l in range(0,N):
    v.append(np.abs(netz.evaluate(x0,l) - np.cos(1)**2))
plt.ylabel('Dropped layer')
plt.xlabel('Layer')
plt.plot(v, 'r')

plt.show()

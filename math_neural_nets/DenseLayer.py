import numpy as np
import afuns
import optimzers

class DenseLayer:
    def __init__(self, ni, no, afun = afuns.ReLU()):
        self.b = np.zeros((no,1))
        self.W = np.random.randn(no, ni) * np.sqrt(1 / (no + ni))
        self.__a = None
        self.__z = None
        self.afun = afun
        self.dW = np.zeros_like(self.W)
        self.db = np.zeros_like(self.b)

    def evaluate(self, a):
        self.__a = a
        self.__z = self.W @ a + self.b
        return self.afun.evaluate(self.__z)

    def backprop(self, delta):
        delta = self.afun.derive(self.__z) * delta
        self.dW = (delta @ np.transpose(self.__a))
        self.db = (delta @ np.ones((delta.shape[1],1)))
        WTdelta = np.transpose(self.W) @ delta
        return WTdelta

    def update(self, optimizer):
        optimizer.update(self)


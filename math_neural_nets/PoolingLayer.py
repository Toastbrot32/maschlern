import numpy as np
import afuns
import optimzers

class PoolingLayer:
    def __init__(self, size):
        self.size = size

    def evaluate(self, a):
        if self.size == (2,2):
            b = np.zeros((a.shape[0], a.shape[1], round(a.shape[2]/2), round(a.shape[3]/2)))
            for i in range(a.shape[0]):
                for j in range(a.shape[1]):
                    b[i,j,:,:] = a[i,j,:,:].reshape(round(a.shape[2]/2), 2, round(a.shape[3]/2), 2).max(axis=(1, 3))
            return b
        else:
            return None

    def backprop(self, delta):
        if self.size == (2,2):
            delta_ = np.zeros((delta.shape[0], delta.shape[1], round(delta.shape[2]*2), round(delta.shape[3]*2)))
            for i in range(delta.shape[0]):
                for j in range(delta.shape[1]):
                    delta_[i,j,:,:] = delta[i,j,:,:].repeat(2).reshape(delta.shape[3],delta.shape[3]*2).T.repeat(2).reshape(delta.shape[3]*2,delta.shape[3]*2).T
            return delta_
        else:
            return None
        pass
        

import numpy as np

class SGD:
    def __init__(self, eta=0.1):
        self.eta=eta
    def update(self, Layer):
        Layer.W = Layer.W - self.eta * Layer.dW
        Layer.b = Layer.b - self.eta * Layer.db

class Adam:
    def __init__(self, beta1=0.9 ,beta2=0.99):
        self.eps = 1e-8
        self.beta1 = beta1
        self.beta2 = beta2

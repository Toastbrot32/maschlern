import matplotlib.pyplot as plt
import tensorflow as tf
from random import randrange
from SequentialNet import *
from afuns import *
from optimzers import *

# training data
mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

# # simple tests
# mb = 20
# x = np.zeros((mb,1,28,28))
# for i in range(mb):
#     x[i,0,:,:] = x_train[i,:,:]
# print("input\n(x, 1, 28, 28) =", x.shape)
# netz = SequentialNet((1,28,28))
# netz.add_conv2im2col((10,3,3), afun=ReLU()) # (10,26,26)
# y = netz.evaluate(x)
# print("(x, 10, 26, 26) =", y.shape)
# for i in range(1):
#     plt.imshow(y[i,0,:,:], cmap='gray')
#     plt.show()
# netz.add_conv2im2col((10,3,3), afun=ReLU()) # (10,24,24)
# y = netz.evaluate(x)
# print("(x, 10, 24, 24) =", y.shape)
# for i in range(1):
#     plt.imshow(y[i,0,:,:], cmap='gray')
#     plt.show()
# netz.add_max_pooling((2,2))           # (10,12,12)
# y = netz.evaluate(x)
# print("(x, 10, 12, 12) =", y.shape)
# for i in range(1):
#     plt.imshow(y[i,0,:,:], cmap='gray')
#     plt.show()
# netz.add_conv2im2col((10,3,3), afun=ReLU()) # (10,10,10)
# y = netz.evaluate(x)
# print("(x, 10, 10, 10) =", y.shape)
# for i in range(1):
#     plt.imshow(y[i,0,:,:], cmap='gray')
#     plt.show()
# netz.add_max_pooling((2,2))           # (10,5,5)
# y = netz.evaluate(x)
# print("(x, 10, 5, 5) =", y.shape)
# for i in range(1):
#     plt.imshow(y[i,0,:,:], cmap='gray')
#     plt.show()
# netz.add_conv2((10,2,2), afun=ReLU()) # (10,4,4)
# y = netz.evaluate(x)
# print("(x, 10, 4, 4) =", y.shape)
# for i in range(1):
#     plt.imshow(y[i,0,:,:], cmap='gray')
#     plt.show()
# netz.add_flattening()                 # 160
# y = netz.evaluate(x)
# print("(160, x) =", y.shape)
# netz.add_dense(10)                    # 10
# y = netz.evaluate(x)
# print("(10, x) =", y.shape)

# net for training: copy of the MNIST conv example
netz = SequentialNet((1,28,28))       # (1,28,28)
netz.add_conv2im2col((32,3,3), afun=ReLU()) # (32,26,26)
#netz.add_conv2((32,3,3), afun=ReLU()) # (32,26,26)
netz.add_flattening()                 # 21632
netz.add_dense(128)                   # 128
netz.add_dense(10)                    # 10

# data for training
batch = 1000
x = np.zeros((batch,1,28,28))
for i in range(batch):
    x[i,0,:,:] = x_train[i,:,:]

# modify data to the shape we need on output
I = np.eye(10)
y_train = I[:, y_train] # one hot encoded
print(y_train.shape)

# training
bs, ep, eta = 10, 10, .01
netz.train(x, y_train[:,:batch], batch_size=bs,
           epochs=ep, optim=SGD(eta=eta))

# predict
samples = 100
x_tilde = np.zeros((samples,1,28,28))
for i in range(samples):
  x_tilde[i,0,:,:] = x_test[i,:,:]

y_tilde = netz.evaluate(x_tilde)
guess = np.argmax(y_tilde, 0).T
print("accuracy =", np.sum(guess == y_test[:samples])/samples*100)

# graphical evaluation
for i in range(4):
    k = randrange(y_tilde.shape[0])
    plt.title('Label is {lb}, guess is {gs}'.format(
        lb=y_test[k], gs=guess[k]))
    plt.imshow(x_test[k], cmap='gray')
    plt.show()

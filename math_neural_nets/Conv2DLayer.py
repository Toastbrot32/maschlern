import numpy as np
import afuns
import optimzers
import scipy.signal

class Conv2DLayer:
    def __init__(self, tensor, filter, afun = afuns.ReLU):
        self.tensor = tensor
        self.c = tensor[0]
        self.h = tensor[1]
        self.w = tensor[2]

        self.filter = filter
        self.m = filter[0]
        self.fh = filter[1]
        self.fw = filter[2]

        self.__a=None
        self.__z=None

        self.f = np.random.randn(self.m,self.c,self.fh,self.fw) * np.sqrt(4 / (self.m*self.c*self.fh*self.fw))
        self.b = np.zeros((self.m))

        self.df = np.zeros_like(self.f)
        self.b = np.zeros_like(self.b)

        self.afun = afun


    def evaluate(self, a):
        self.__a = a
        z = np.zeros((a.shape[0], self.m, (a.shape[2] - self.fh +1), (a.shape[3] - self.fw +1)))
        for i in range(a.shape[0]):
            for j in range(self.m):
                z[i,j,:,:] = z[i,j,:,:] + self.b[j]
                for k in range(self.c):
                    z[i,j,:,:] = z[i,j,:,:] + scipy.signal.convolve2d(a[i,k,:,:], self.f[j,k,:,:], mode='valid')
        self.__z=z
        return self.afun.evaluate(z)

    def backprop(self, delta):
        delta = self.afun.derive(self.__z) * delta

        s = scipy.signal.convolve2d(np.flip(delta[0,0,:,:],(0,1)), self.f[0,0,:,:], mode='full').shape
        WTdelta = np.zeros((delta.shape[0], delta.shape[1], s[0], s[1]))
        for i in range(delta.shape[0]):
            for k in range(self.c):
                for j in range(self.m):
                    WTdelta[i,k,:,:] = WTdelta[i,k,:,:] + scipy.signal.convolve2d(np.flip(delta[i,j,:,:],(0,1)), self.f[j,k,:,:], mode='full')

        self.db = np.zeros(self.m)
        self.df = np.zeros_like(self.f)
        for j in range(self.m):
            self.db[j] = np.sum(delta[:,j,:,:])
            for k in range(self.c):
                for i in range(delta.shape[0]):
                    self.df[j,k,:,:] += scipy.signal.convolve2d(self.__a[i,k,:,:], np.flip(delta[i,j,:,:],[0,1]), mode = 'valid')[::-1,::-1]


        return WTdelta

    def update(self, optim):
        eta = 0.9
        self.f = self.f - eta * self.df
        self.b = self.b - eta * self.db



A = Conv2DLayer((3,23,4),(123,3,4))
A.evaluate

import numpy as np
strided = np.lib.stride_tricks.as_strided
import scipy.signal as signal

def im2col(a, f1, f2):
    n, c, a1, a2 = a.shape
    ns, cs, a1s, a2s = a.strides
    z1 = a1 - f1 + 1
    z2 = a2 - f2 + 1
    col_pre = strided(a,
                      (c, f1, f2, n, z1, z2),
                      (cs, a1s, a2s, ns, a1s, a2s))
    col = col_pre.reshape(np.multiply.reduceat(col_pre.shape, (0, 3)))
    return col_pre, col


def im2row(a, f1, f2):
    n, c, a1, a2 = a.shape
    ns, cs, a1s, a2s = a.strides
    z1 = a1 - f1 + 1
    z2 = a2 - f2 + 1
    row_pre = strided(a,
                      (n, z1, z2, c, f1, f2),
                      (ns, a1s, a2s, cs, a1s, a2s))
    row = row_pre.reshape(np.multiply.reduceat(row_pre.shape, (0, 3)))
    return row


a = np.arange(2*2*3*3).reshape(2, 2, 3, 3)
f= np.arange(2*2*2*2).reshape(2, 2, 2, 2)

col_pre, col = im2col(a, 2, 2)

row = im2row(f[:,:,::-1,::-1], 2, 2)

print(col)
print(col_pre)
print((row @ col).reshape(2,2,2,2))

print(signal.convolve2d(a[0,0,:,:],f[0,0,:,:],mode='valid') +  signal.convolve2d(a[0,1,:,:],f[0,1,:,:],mode='valid'))

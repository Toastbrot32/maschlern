import numpy as np
import afuns
import optimzers
import scipy.signal
strided = np.lib.stride_tricks.as_strided


def col2im(B, a, f1, f2):
    n, c, a1, a2 = a.shape
    ns, cs, a1s, a2s = a.strides
    z1 = a1 - f1 + 1
    z2 = a2 - f2 + 1
    B = B.reshape(c, f1, f2, n, z1, z2)
    return strided(B,a.shape,a.strides)

def im2col(a, f1, f2):
    n, c, a1, a2 = a.shape
    ns, cs, a1s, a2s = a.strides
    z1 = a1 - f1 + 1
    z2 = a2 - f2 + 1
    col_pre = strided(a,
                      (c, f1, f2, n, z1, z2),
                      (cs, a1s, a2s, ns, a1s, a2s))
    col = col_pre.reshape(np.multiply.reduceat(col_pre.shape, (0, 3)))
    return col_pre, col


def im2row(a, f1, f2):
    n, c, a1, a2 = a.shape
    ns, cs, a1s, a2s = a.strides
    z1 = a1 - f1 + 1
    z2 = a2 - f2 + 1
    row_pre = strided(a,
                      (n, z1, z2, c, f1, f2),
                      (ns, a1s, a2s, cs, a1s, a2s))
    row = row_pre.reshape(np.multiply.reduceat(row_pre.shape, (0, 3)))
    return row


class Conv2DLayerim2col:
    def __init__(self, tensor, filter, afun = afuns.ReLU):

        self.c = tensor[0]
        self.h = tensor[1]
        self.w = tensor[2]

        self.m = filter[0]
        self.fh = filter[1]
        self.fw = filter[2]

        self.b = np.zeros((1,self.m))
        self.f = np.random.randn(self.m,self.c,self.fh,self.fw) * np.sqrt(4 / (self.m*self.c*self.fh*self.fw))

        self.__a = None
        self.__z = None
        self.afun = afun
        self.df = np.zeros_like(self.f)
        self.db = np.zeros_like(self.b)

    def evaluate(self, a):
        col_pre, col = im2col(a, self.fh, self.fw)
        row = im2row(self.f[:,:,::-1,::-1], self.fh, self.fw)
        self.__a = a
        self.__z = row @ col + self.b.T
        return self.afun.evaluate(self.__z).reshape(a.shape[0], self.m, (a.shape[2] - self.fh +1), (a.shape[3] - self.fw +1))

    def backprop(self, delta):
        #db = np.sum(delta, axis=(0, 2, 3))
        z=self.__z
        delta = self.afun.derive(self.__z) * delta.reshape(self.__z.shape)
        row = im2row(self.f[:,:,::-1,::-1], self.fh, self.fw)

        a=self.__a
        Ndelta = col2im(row.T@delta, self.__a, self.fh, self.fw)
        return Ndelta

    def update(self, optim):
        pass


import numpy as np
import afuns
import optimzers

class FlattenLayer:
    def __init__(self):
        self.n = 0
        self.c = 0
        self.h = 0
        self.w = 0

    def evaluate(self, a):
        self.n=a.shape[0]
        self.c=a.shape[1]
        self.h=a.shape[2]
        self.w=a.shape[3]
        return a.reshape(a.shape[0], a.shape[1]*a.shape[2]*a.shape[3]).T

    def backprop(self, delta):
        return delta.reshape(self.n, self.c, self.h, self.w)

    def update(self, optim):
        pass

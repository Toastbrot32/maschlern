import numpy as np

class ReLU:
    def __init__(self):
        pass
    def evaluate(self, x):
        return np.maximum(0,x)
    def derive(self, x):
        return np.heaviside(x,1)

class absval:
    def __init__(self):
        pass
    def evaluate(self, x):
        return np.abs(x)
    def derive(self, x):
        return np.sign(x)

class ELU:
    def __init__(self, ELU_alpha=0.1):
        self.ELU_alpha = ELU_alpha
    def evaluate(self, x):
        return np.where(x>0,x,0) + self.ELU_alpha*(np.exp(np.where(x<=0,x,0))-1)
    def derive(self, x):
        return np.where(x>0,1,0) + self.ELU_alpha*(np.exp(np.where(x<=0,x,0)))


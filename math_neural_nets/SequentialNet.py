import numpy as np
import DenseLayer
import Conv2DLayer
import Conv2DLayerim2col
import afuns
import optimzers
import PoolingLayer
import FlattenLayer

class SequentialNet:
    def __init__(self, input):
        self.inputsize = input
        self.layers = []
        self.no = np.array(input)

    def add_dense(self, n, afun=afuns.ReLU()):
        self.layers.append(DenseLayer.DenseLayer(self.no, n, afun))
        self.no = n

    def add_conv2(self, n, afun=afuns.ReLU()):
        self.layers.append(Conv2DLayer.Conv2DLayer(self.no, n, afun))
        self.no = (n[0], self.no[1] - n[1] + 1, self.no[2] -n[2] + 1)


    def add_conv2im2col(self, n, afun=afuns.ReLU()):
        self.layers.append(Conv2DLayerim2col.Conv2DLayerim2col(self.no, n, afun))
        self.no = (n[0], self.no[1] - n[1] + 1, self.no[2] -n[2] + 1)

    def add_max_pooling(self, size):
        self.layers.append(PoolingLayer.PoolingLayer(size))
        self.no = (self.no[0], int(self.no[1]/2), int(self.no[2]/2))

    def add_flattening(self):
        self.layers.append(FlattenLayer.FlattenLayer())
        self.no = int(self.no[0] * self.no[1] * self.no[2])

    def evaluate(self, x):
        num_layers = len(self.layers)
        for i in range(0,num_layers):
            x = self.layers[i].evaluate(x)
        return x

    def backprop(self, x, y):
        k = y.shape[1]
        num_layers = len(self.layers)
        a = self.evaluate(x)
        delta = (a - y) /k
        #print(f'Loss {np.linalg.norm(delta)}', end="\r")
        for i in reversed(range(0,num_layers)):
            delta = self.layers[i].backprop(delta)
        return delta

    def train(self, x, y, batch_size=16, epochs=10, optim=optimzers.SGD(eta=0.1)):
        num_batches = int(np.ceil(y.shape[1] / batch_size))
        num_layers = len(self.layers)
        for i in range(0,epochs):
            print(f'SGD epoch {i+1} von {epochs} Epochen', end="\r")
            p=np.random.permutation(y.shape[1])
            for k in range(0,num_batches):
                #self.backprop(x[:,p[k*batch_size:(k+1)*batch_size]],y[:,p[k*batch_size:(k+1)*batch_size]])
                self.backprop(x[p[k*batch_size:(k+1)*batch_size],:],y[:,p[k*batch_size:(k+1)*batch_size]])
                for j in range(0,num_layers):
                    self.layers[j].update(optim)
            print("                                                                   ", end="\r")


#A = SequentialNet(2)
#A.add_dense(3)
#A.add_dense(5)
#print(A.evaluate(np.array([[1],[1]])))

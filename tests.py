from FeedforwardNetwork import FeedforwardNet
from mlxtend.data import loadlocal_mnist
import numpy as np

x=np.array([[0., 0., 1., 1.],
            [0., 1., 0., 1.]])
y=np.array([[0., 1., 1., 1.],[0., 0., 0., 1.]])

A=FeedforwardNet([2,10,2],3*["ELU"])
A.train( x, y, eta=0.01, batch_size=2, epochs=1000, GDVersion="SGD")

B=FeedforwardNet([2,10,2],3*["ELU"])
B.train( x, y, eta=0.1, batch_size=2, epochs=1000, GDVersion="SGD")

C=FeedforwardNet([2,10,2],3*["ELU"])
C.train( x, y, eta=0.1, batch_size=2, epochs=1000, GDVersion="SGD")

print("Adagrad",A.evaluate(x))
print("SGD",B.evaluate(x))
print("SGDM",C.evaluate(x))

C.draw()
#A.draw()

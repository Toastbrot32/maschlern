from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
import pickle
import os
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt

### Create Dictionaries to reduce output space dimension
sym2intf = open('symbols.csv')
sym2intf.readline()
sym2int = {}
i=0
for line in sym2intf:
    V = line.split(',')
    sym2int[int(V[0])] = i
    i = i+1

num_labels = i
int2sym = dict(map(reversed, sym2int.items()))

data = open('traina.pickle', 'rb')
xtrain, ytrain, trainsamples = pickle.load(data)

data = open('test.pickle', 'rb')
xtest, ytest, testsamples = pickle.load(data)

#1 4 conv layer
model1 = keras.Sequential([
    keras.layers.Conv2D(32, 3, padding='same', input_shape=(32,32,1)),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(32, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Conv2D(64, 7, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(64, 7, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Flatten(),

    keras.layers.Dense(2048),
    keras.layers.Dropout(0.5),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Dense(2048),
    keras.layers.Dropout(0.5),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Dense(num_labels),
    keras.layers.Activation('softmax'),
])

model1.compile(optimizer='adam',
              loss='mean_squared_error',
              metrics=['accuracy', 'top_k_categorical_accuracy'])

#2 6 conv layer
model2 = keras.Sequential([
    keras.layers.Conv2D(32, 3, padding='same', input_shape=(32,32,1)),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(32, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Conv2D(32, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(32, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Conv2D(64, 7, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(64, 7, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Flatten(),

    keras.layers.Dense(2048),
    keras.layers.Dropout(0.3),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Dense(2048),
    keras.layers.Dropout(0.3),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Dense(num_labels),
    keras.layers.Activation('softmax'),
])

model2.compile(optimizer='adam',
              loss='mean_squared_error',
              metrics=['accuracy', 'top_k_categorical_accuracy'])

#3 8 conv layer 
model3 = keras.Sequential([
    keras.layers.Conv2D(32, 2, padding='same', input_shape=(32,32,1)),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(32, 2, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Conv2D(32, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(32, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Conv2D(64, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(64, 3, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Conv2D(128, 5, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Conv2D(128, 5, padding='same'),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),

    keras.layers.MaxPooling2D(pool_size=2, strides=None, padding='same'),

    keras.layers.Flatten(),

    keras.layers.Dense(2048),
    keras.layers.Dropout(0.5),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Dense(2048),
    keras.layers.Dropout(0.5),
    keras.layers.BatchNormalization(),
    keras.layers.Activation('relu'),
    keras.layers.Dense(num_labels),
    keras.layers.Activation('softmax'),
])

model3.compile(optimizer='adam',
              loss='mean_squared_error',
              metrics=['accuracy', 'top_k_categorical_accuracy'])

# modelver = '1'

# history = model1.fit(xtrain.reshape(trainsamples,32,32,1), ytrain, workers=4, epochs=100, batch_size=100, validation_split=0.05)

# np.savetxt(f'model{modelver}_acc.txt', history.history['accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_valacc.txt', history.history['val_accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_loss.txt', history.history['loss'], delimiter=',')
# np.savetxt(f'model{modelver}_valloss.txt', history.history['val_loss'], delimiter=',')
# np.savetxt(f'model{modelver}_top5.txt', history.history['top_k_categorical_accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_valtop5.txt', history.history['val_top_k_categorical_accuracy'], delimiter=',')

# model1.save(f'model{modelver}') 

# modelver = '2'

# history = model2.fit(xtrain.reshape(trainsamples,32,32,1), ytrain, workers=4, epochs=100, batch_size=100, validation_split=0.05)

# np.savetxt(f'model{modelver}_acc.txt', history.history['accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_valacc.txt', history.history['val_accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_loss.txt', history.history['loss'], delimiter=',')
# np.savetxt(f'model{modelver}_valloss.txt', history.history['val_loss'], delimiter=',')
# np.savetxt(f'model{modelver}_top5.txt', history.history['top_k_categorical_accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_valtop5.txt', history.history['val_top_k_categorical_accuracy'], delimiter=',')

# model2.save(f'model{modelver}') 

modelver = '3'

history = model3.fit(xtrain.reshape(trainsamples,32,32,1), ytrain, workers=4, epochs=100, batch_size=100, validation_split=0.05)

# np.savetxt(f'model{modelver}_acc.txt', history.history['accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_valacc.txt', history.history['val_accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_loss.txt', history.history['loss'], delimiter=',')
# np.savetxt(f'model{modelver}_valloss.txt', history.history['val_loss'], delimiter=',')
# np.savetxt(f'model{modelver}_top5.txt', history.history['top_k_categorical_accuracy'], delimiter=',')
# np.savetxt(f'model{modelver}_valtop5.txt', history.history['val_top_k_categorical_accuracy'], delimiter=',')

model3.save(f'model{modelver}') 

print()
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

class partialImage:
    def __init__(self, img, x, y):
        self.img = img
        self.x = x
        self.y = y



img = cv.imread('test.png').astype(np.uint8)
img = cv.blur(img,(10,10))
imgbw = cv.cvtColor(img,cv.COLOR_RGB2GRAY)
ret, imgt = cv.threshold(imgbw,253,254,cv.THRESH_BINARY_INV)

plt.imshow(imgt)
plt.show()

num, CC, stats, centroids = cv.connectedComponentsWithStats(imgt.astype(np.uint8),8,cv.CV_32S)

print(centroids)

for j in range(1,num):
    A = (CC == j)
    w = int(max(stats[j][2], stats[j][3])/2 + 10)
    x = int(round(centroids[j][1]))
    y = int(round(centroids[j][0]))
    P = A[x-w:x+w, y-w:y+w].astype(np.uint8)
    resized = cv.resize(P, (32,32), 0,0,interpolation = cv.INTER_AREA)
    plt.imshow(resized)
    plt.savefig(f'{j}.png') 
    plt.show()

print()
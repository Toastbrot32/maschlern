from vgw import game
import numpy as np
import random

from keras.models import Sequential
from keras.layers import Dense, Activation

Game = game()

trainx = []
trainy = []

for j in range(0,10000):
    for i in range(0,2):
        M = Game.possiblemoves()
        Ml = len(M)
        m = M[random.randint(0,Ml-1)]
        if i==0:
            Game.play('X',m)
        if i==1:
            Game.play('O',m)

        if Game.won() == 'X':
            trainx.append(Game.board.copy().flatten())
            #trainx.append(Game.board.copy())
            trainy.append(1000)
            #print('X won')
            Game = game()

        elif Game.won() == 'O':
            trainx.append(Game.board.copy().flatten())
            #trainx.append(Game.board.copy())
            trainy.append(0)
            #print('O won')
            Game = game()

        elif Game.won() == 'S':
            trainx.append(Game.board.copy().flatten())
            #trainx.append(Game.board.copy())
            trainy.append(10)
            #print('Stalemate')
            Game = game()

        #else:
            #trainx.append(Game.board.copy().flatten())
            #trainx.append(Game.board.copy())
            #trainy.append(0)


model = Sequential()
model.add(Dense(100, activation='relu', input_shape=(6*7,)))
model.add(Dense(100, activation='relu'))
model.add(Dense(100, activation='relu'))
model.add(Dense(100, activation='relu'))
model.add(Dense(1, activation='relu'))
model.compile(optimizer='rmsprop',
              loss='binary_crossentropy',
              metrics=['accuracy'])

model.fit(np.asarray(trainx), np.asarray(trainy), epochs=10, batch_size=10)

for i in range(0,10):
    Game = game()
    for j in range(0,7):
        M = Game.possiblemoves()
        Ml = len(M)
        m = M[random.randint(0,Ml-1)]
        Game.play('X',m)
        if Game.won() == 'X':
            break

        M = Game.possiblemoves()
        Ml = len(M)
        m = M[random.randint(0,Ml-1)]
        Game.play('O',m)
        if Game.won() == 'O':
            break

    print(Game.won())
    Game.print()
    print(model.predict(Game.board.reshape(1,42)))
    print('---')

import numpy as np
from scipy import signal

A = np.zeros((6,7))

class game:
    def __init__(self, n = 6, m = 7):
        self.board = np.zeros((n,m))

    def print(self):
        print('_____________________')
        for i in range(0,6):
            for j in range(0,7):
                if self.board[i,j] == 1:
                    print('|X|', end = '')
                elif self.board[i,j] == -1:
                    print('|O|', end = '')
                else:
                    print('|_|', end = '')
            print('')
        print('¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯')
            

    def play(self, player, row):
        if not self.movepossible(row):
            return False

        if player == 'X':
            for i in range(0,6):
                if self.board[i,row] != 0:
                    self.board[i-1,row] = 1
                    return
                elif i==5:
                    self.board[5,row] = 1
                    return

        if player == 'O':
            for i in range(0,6):
                if self.board[i,row] != 0:
                    self.board[i-1,row] = -1
                    return
                elif i==5:
                    self.board[5,row] = -1
                    return

    def movepossible(self, row):
        if self.board[0,row] == 0:
            return True
        else:
            return False

    def possiblemoves(self):
        moves = []
        for i in range(0, 7):
            if self.movepossible(i):
                moves.append(i)
        return moves

    def won(self):
        PlayerX = np.where(self.board != 1, 0, self.board)
        PlayerO = np.where(self.board != -1, 0, self.board)

        Vert = np.ones((4,1))
        Hor = np.ones((1,4))
        LR = np.eye(4)
        RL = np.fliplr(np.eye(4))

        PX = max([np.max(signal.convolve2d(PlayerX, Vert, 'valid')), np.max(signal.convolve2d(PlayerX, Hor, 'valid')), np.max(signal.convolve2d(PlayerX, LR, 'valid')), np.max(signal.convolve2d(PlayerX, RL, 'valid'))])
        PO = max([np.max(signal.convolve2d(PlayerO, -Vert, 'valid')), np.max(signal.convolve2d(PlayerO, -Hor, 'valid')), np.max(signal.convolve2d(PlayerO, -LR, 'valid')), np.max(signal.convolve2d(PlayerO, -RL, 'valid'))])

        if PX == 4:
            return 'X'

        if PO == 4:
            return 'O'

        if any(list(map(self.movepossible,range(0,7)))) == False:
            return 'S'

        return 'P'


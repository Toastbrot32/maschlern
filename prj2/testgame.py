from vgw import game
import random

Game = game()

while True:
    M = Game.possiblemoves()
    Ml = len(M)
    m = M[random.randint(0,Ml-1)]
    Game.play('X',m)
    Game.print()
    if Game.won() == 'X':
        break

    M = Game.possiblemoves()
    Ml = len(M)
    m = M[random.randint(0,Ml-1)]
    Game.play('O',m)
    Game.print()
    if Game.won() == 'O':
        break

Game.print()
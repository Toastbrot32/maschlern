import numpy as np
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
import itertools

class FeeddorwardNet:
    def __init__(self, n, w, b):
        m = np.size(n)
        self.n = n

        self.afuns = []
        for i in range(0, m-1):
            self.afuns.append(lambda x:(np.maximum(0,x)))

        self.biasvecs = []
        for i in range(1, m):
            self.biasvecs.append(b[i-1])

        self.weights = []
        for i in range(0, m-1):
            self.weights.append(w[i])

    def evaluate(self,x):
        for i in range(0,np.size(self.n)-1):
            x=self.weights[i]@x+self.biasvecs[i]
            x=self.afuns[i](x)
        return x

    def draw(self, **kwargs):
        fig, ax = plt.subplots()
        for j in range(0,np.size(self.n)):
            for i in range(0, self.n[j]):
                pos = (4 * j, (4 * i) - 2 * self.n[j])
                if not j == np.size(self.n)-1:
                    for h in range (0, self.n[j+1]):
                        pos2 = (4 * (j+1), (4 * h) - 2 * self.n[j+1])
                        line = plt.Line2D((pos[0],pos2[0]), (pos[1],pos2[1]), lw = 1, zorder = 0.0)
                        pos3 = np.array(pos) + ((h+1)/(self.n[j+1]+1)) * (np.array(pos2) - np. array(pos))
                        ax.add_line(line)
                        if 'PrintAnnotations' in kwargs:
                            if kwargs['PrintAnnotations']==True:
                                ax.annotate(str(round(self.weights[j][h,i],2)), pos3, zorder = 100.0)
                circle = plt.Circle(pos, radius = 0.5, fc = 'r', zorder = 10.0)
                ax.add_patch(circle)
                if 'PrintAnnotations' in kwargs:
                    if kwargs['PrintAnnotations']==True:
                        if not j == 0:
                            ax.annotate(str(round(self.biasvecs[j-1][i,0],2)), pos, zorder = 1000.0)
        plt.axis('scaled')
        plt.show()

#A=FeeddorwardNet([2,2,1], [np.array([[9,9],[9,9]]), np.array([[9,9]])], [np.array([[-17], [2]]), np.array([[-1]])])
#A=FeeddorwardNet([2,1], [np.array([[1,1]])], [np.array([[-1.0]])])
#OR
A=FeeddorwardNet([2,2,1], [np.array([[1,1],[1,1]]), np.array([[1,-1]])], [np.array([[0], [-1]]), np.array([[0]])])
#XOR
B=FeeddorwardNet([2,2,1], [np.array([[1,1],[1,1]]), np.array([[1,-2]])], [np.array([[0], [-1]]), np.array([[0]])])
#AND
C=FeeddorwardNet([2,2,1], [np.array([[1,1],[1,1]]), np.array([[0,1]])], [np.array([[0], [-1]]), np.array([[0]])])
print(A.afuns)
print(A.biasvecs)
print(A.weights)
print(A.evaluate(np.array([[1, 1, 0, 0],[1, 0, 1, 0]])))
print(np.array_equal(np.array([[1,0,0,0]]), A.evaluate(np.array([[1, 1, 0, 0],[1, 0, 1, 0]]))))
print(A.evaluate(np.array([[1],[0]])))
print(A.evaluate(np.array([[0],[1]])))
print(A.evaluate(np.array([[0],[0]])))
#C.draw(PrintAnnotations=True)

for perms in itertools.product(range(-5,5), repeat=3):
    A=FeeddorwardNet([2,1], [np.array([[perms[0],perms[1]]])], [np.array([[perms[2]]])])
    if np.array_equal([[1]], A.evaluate(np.array([[1],[1]]))) and np.array_equal([[0]], A.evaluate(np.array([[1],[0]]))) and  np.array_equal([[0]], A.evaluate(np.array([[0],[1]]))) and np.array_equal([[0]], A.evaluate(np.array([[0],[0]]))):
        print(A.weights)
        print(A.biasvecs)

print('---------------')

for perms in itertools.product(range(-2,2),repeat=9):
    A=FeeddorwardNet([2,2,1], [np.array([[perms[0],perms[1]],[perms[2],perms[3]]]), np.array([[perms[4],perms[5]]])], [np.array([[perms[6]], [perms[7]]]), np.array([[perms[8]]])])
    if np.array_equal([[1]], A.evaluate(np.array([[1],[1]]))) and np.array_equal([[1]], A.evaluate(np.array([[1],[0]]))) and  np.array_equal([[1]], A.evaluate(np.array([[0],[1]]))) and np.array_equal([[0]], A.evaluate(np.array([[0],[0]]))):
        print(A.weights)
        print(A.biasvecs)
        break

print('---------------')

for perms in itertools.product(range(-2,2),repeat=9):
    A=FeeddorwardNet([2,2,1], [np.array([[perms[0],perms[1]],[perms[2],perms[3]]]), np.array([[perms[4],perms[5]]])], [np.array([[perms[6]], [perms[7]]]), np.array([[perms[8]]])])
    if np.array_equal([[0]], A.evaluate(np.array([[1],[1]]))) and np.array_equal([[1]], A.evaluate(np.array([[1],[0]]))) and  np.array_equal([[1]], A.evaluate(np.array([[0],[1]]))) and np.array_equal([[0]], A.evaluate(np.array([[0],[0]]))):
        print(A.weights)
        print(A.biasvecs)
        break
